
import os
import numpy as np
import cv2
from glob import glob
import tensorflow as tf
from keras_preprocessing.image import ImageDataGenerator
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau, CSVLogger, TensorBoard
from data import load_data, tf_dataset, tf_dataset_dual, tf_dataset_multi
from model import build_model, build_model_dual, build_model_multi
from keras import backend as K
from tensorflow.keras.layers import *
from tensorflow.keras.models import Model



def dice(y_true, y_pred):
    """
    calculates the Dice coeffiecient of the GT and predicted masks
    """
    def recall(y_true, y_pred):
        """Recall metric.

        Only computes a batch-wise average of recall.

        Computes the recall, a metric for multi-label classification of
        how many relevant items are selected.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

    def precision(y_true, y_pred):
        """Precision metric.

        Only computes a batch-wise average of precision.

        Computes the precision, a metric for multi-label classification of
        how many selected items are relevant.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision

    precision = precision(y_true, y_pred)
    recall = recall(y_true, y_pred)
    return (2 * (precision * recall) + K.epsilon())/ (precision + recall + K.epsilon())

def iou(y_true, y_pred):
    """
    calculates the intersection over union of the GT and predicted masks.
    """
    def f(y_true, y_pred):
        intersection = (y_true * y_pred).sum()
        union = y_true.sum() + y_pred.sum() - intersection
        x = (intersection + 1e-15) / (union + 1e-15)
        x = x.astype(np.float32)
        return x
    return tf.numpy_function(f, [y_true, y_pred], tf.float32)

if __name__ == "__main__":
    ## Dataset
    path = "/home/moazemi/data/dataset/dataset/"
    train_paths = []
    train_paths.append("/home/moazemi/data/train/responders")
    train_paths.append("/home/moazemi/data/train/non_responders")
    train_ids = []
    for dest_path in train_paths:
        for id, patient_id in enumerate(sorted(os.listdir(dest_path))):
            train_ids.append(int(patient_id))

    print ("Num. of Training Subjects = ", len(train_ids))
    (train_x, train_y), (valid_x, valid_x_ct, valid_th, valid_bone, valid_y), (test_x, test_y), (train_th, test_th), \
    (train_x_ct, test_x_ct), (train_bone, test_bone) = load_data(path, id_range=sorted(train_ids))

    ## Hyperparameters
    batch = 16
    lr = 0.001
    epochs = 60
    input_shape = (256, 256, 1)

    # Training only based on PET channel
    # train_dataset = tf_dataset(train_x, train_y, batch=batch)
    # valid_dataset = tf_dataset(valid_x, valid_y, batch=batch)

    # Training based on PET and CT channels
    train_dataset = tf_dataset_dual(train_x, train_x_ct, train_y, batch=batch)
    valid_dataset = tf_dataset_dual(valid_x, valid_x_ct, valid_y, batch=batch)

    # Training based on 3 channels (PET, CT, thresholding)
    # train_dataset = tf_dataset_multi(train_x, train_x_ct, train_bone, train_y, batch=batch)
    # valid_dataset = tf_dataset_multi(valid_x, valid_x_ct, valid_bone, valid_y, batch=batch)

    # Building the model
    model = build_model_dual(output_bias=np.log(1./99.))

    # Setting up the model optimizer and performance metrics
    opt = tf.keras.optimizers.Adam(lr)
    metrics = ["acc", tf.keras.metrics.Recall(), tf.keras.metrics.Precision(), dice]
    model.compile(loss="binary_crossentropy", optimizer=opt, metrics=metrics)

    # Setting up the callbacks
    callbacks = [
        ModelCheckpoint("/home/moazemi/src/autopypetct/useg/files/model.h5"),
        ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=4),
        CSVLogger("/home/moazemi/src/autopypetct/useg/files/data_NEW_PETCT_binary_cross_entropy_NEW.csv"),
        TensorBoard(),
        EarlyStopping(monitor='val_loss', patience=10, restore_best_weights=False)
    ]

    train_steps = len(train_x)//batch
    valid_steps = len(valid_x)//batch

    if len(train_x) % batch != 0:
        train_steps += 1
    if len(valid_x) % batch != 0:
        valid_steps += 1

    # Fitting the model
    model.fit(train_dataset,
        validation_data=valid_dataset,
        epochs=epochs,
        steps_per_epoch=train_steps,
        validation_steps=valid_steps,
        callbacks=callbacks)
