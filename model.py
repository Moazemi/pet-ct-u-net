
import tensorflow as tf
from tensorflow.keras.layers import *
from tensorflow.keras.models import Model

def conv_block(x, num_filters):
    x = Conv2D(num_filters, (3, 3), padding="same")(x)
    x = BatchNormalization()(x)
    x = Activation("relu")(x)

    x = Conv2D(num_filters, (3, 3), padding="same")(x)
    x = BatchNormalization()(x)
    x = Activation("relu")(x)

    return x

def build_model(output_bias=None):
    size = 256
    num_filters = [16, 32, 48, 64, 128, 256, 480, 512]
    inputs = Input((size, size, 1))

    skip_x = []
    x = inputs
    ## Encoder
    for f in num_filters:
        x = conv_block(x, f)
        skip_x.append(x)
        x = MaxPool2D((2, 2))(x)

    ## Bridge
    x = conv_block(x, num_filters[-1])

    num_filters.reverse()
    skip_x.reverse()
    ## Decoder
    for i, f in enumerate(num_filters):
        x = UpSampling2D((2, 2))(x)
        xs = skip_x[i]
        x = Concatenate()([x, xs])
        x = conv_block(x, f)

    ## Output
    x = Conv2D(1, (1, 1), padding="same")(x)
    x = Dense(1, activation="sigmoid", bias_initializer=output_bias)(x)

    return Model(inputs, x)



def build_model_dual(output_bias=None):
    if output_bias is not None:
        output_bias = tf.keras.initializers.Constant(output_bias)
    size = 256
    num_filters = [16, 32, 48, 64, 128, 256, 480, 512]
    pet_inputs = Input((size, size, 1))
    ct_inputs = Input((size, size, 1))

    skip_x = []
    x = pet_inputs
    x_ct = ct_inputs

    x = Concatenate(-1)((x, x_ct))
    ## Encoder
    for f in num_filters:
        x = conv_block(x, f)
        skip_x.append(x)
        x = MaxPool2D((2, 2))(x)

    ## Bridge
    x = conv_block(x, num_filters[-1])

    num_filters.reverse()
    skip_x.reverse()
    ## Decoder
    for i, f in enumerate(num_filters):
        x = UpSampling2D((2, 2))(x)
        xs = skip_x[i]
        x = Concatenate()([x, xs])
        x = conv_block(x, f)

    ## Output
    x = Conv2D(1, (1, 1), padding="same")(x)
    x = Dense(1, activation="sigmoid", bias_initializer=output_bias)(x)

    return Model([pet_inputs, ct_inputs], x)

def build_model_multi(output_bias=None):
    size = 256
    num_filters = [16, 32, 48, 64, 128, 256, 480, 512]
    pet_inputs = Input((size, size, 1))
    ct_inputs = Input((size, size, 1))
    bone_inputs = Input((size, size, 1))

    skip_x = []
    x = pet_inputs
    x_ct = ct_inputs
    x_thres = bone_inputs

    x = Concatenate(-1)((x, x_ct, bone_inputs))
    ## Encoder
    for f in num_filters:
        x = conv_block(x, f)
        skip_x.append(x)
        x = MaxPool2D((2, 2))(x)

    ## Bridge
    x = conv_block(x, num_filters[-1])

    num_filters.reverse()
    skip_x.reverse()
    ## Decoder
    for i, f in enumerate(num_filters):
        x = UpSampling2D((2, 2))(x)
        xs = skip_x[i]
        x = Concatenate()([x, xs])
        x = conv_block(x, f)

    ## Output
    x = Conv2D(1, (1, 1), padding="same")(x)
    x = Dense(1, activation="sigmoid", bias_initializer=output_bias)(x)

    return Model([pet_inputs, ct_inputs, bone_inputs], x)

if __name__ == "__main__":
    model = build_model()
    model.summary()
    dot_img_file = 'model_1.png'
    tf.keras.utils.plot_model(model, to_file=dot_img_file, show_shapes=True, show_layer_names=False, rankdir='TB', expand_nested=True, dpi=150)
