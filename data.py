
import os
import numpy as np
import cv2
from glob import glob
import tensorflow as tf
from sklearn.model_selection import train_test_split

def load_data(path, id_range=[], split=0.1):
    """
    Use to load the training dataset.
    Use id_range to specify the training id range for an input path containing multiple subjects.
    Otherwise, the specified path should correspond to a single subject
    """
    images = []
    images_ct = []
    masks = []
    thres = []
    bones = []
    if id_range:
        for patient_id in id_range:
            _images = []
            _images_ct = []
            _masks = []
            _thres = []
            _bones = []
            patient_id = "%2.f" % patient_id
            _images = sorted(glob(os.path.join(path, patient_id, "dicom/pet/images/*")))
            _images_ct = sorted(glob(os.path.join(path, patient_id, "dicom/ct/images/*")))
            _masks = sorted(glob(os.path.join(path, patient_id, "mediso/masks/*")))
            _thres = sorted(glob(os.path.join(path, patient_id, "dicom/pet/thres/*")))
            _bones = sorted(glob(os.path.join(path, patient_id, "dicom/ct/bone_masks/*")))
            num_slices = min(len(_images), len(_images_ct))
            for i in range(num_slices):
                images.append(_images[i])
                images_ct.append(_images_ct[i])
                masks.append(_masks[i])
                thres.append(_thres[i])
                bones.append(_bones[i])
    else:
        images = sorted(glob(os.path.join(path, "dicom/pet/images/*")))
        images_ct = sorted(glob(os.path.join(path, "dicom/ct/images/*")))
        masks = sorted(glob(os.path.join(path, "mediso/masks/*")))
        thres = sorted(glob(os.path.join(path, "dicom/pet/thres/*")))
        bones = sorted(glob(os.path.join(path, "dicom/ct/bone_masks/*")))

    total_size = len(images)
    valid_size = int(split * total_size)
    test_size = int(split * total_size)


    train_x_ct, valid_x_ct = train_test_split(images_ct, test_size=valid_size, random_state=42)
    train_x, valid_x = train_test_split(images, test_size=valid_size, random_state=42)
    train_y, valid_y = train_test_split(masks, test_size=valid_size, random_state=42)
    train_th, valid_th = train_test_split(thres, test_size=valid_size, random_state=42)
    train_bone, valid_bone = train_test_split(bones, test_size=valid_size, random_state=42)

    train_x_ct, test_x_ct = train_test_split(train_x_ct, test_size=test_size, random_state=42)
    train_x, test_x = train_test_split(train_x, test_size=test_size, random_state=42)
    train_y, test_y = train_test_split(train_y, test_size=test_size, random_state=42)
    train_th, test_th = train_test_split(train_th, test_size=test_size, random_state=42)
    train_bone, test_bone = train_test_split(train_bone, test_size=test_size, random_state=42)

    return (train_x, train_y), (valid_x, valid_x_ct, valid_th, valid_bone, valid_y), (test_x, test_y), \
           (train_th, test_th), (train_x_ct, test_x_ct), (train_bone, test_bone)


def load_test_data(path):
    """
    Use to load test dataset. The path should include data from a single subject
    """
    images = sorted(glob(os.path.join(path, "dicom/pet/images/*")))
    images_ct = sorted(glob(os.path.join(path, "dicom/ct/images/*")))
    masks = sorted(glob(os.path.join(path, "mediso/masks/*")))
    thres = sorted(glob(os.path.join(path, "dicom/pet/thres/*")))
    preds = sorted(glob(os.path.join(path, "pred/*")))
    bones = sorted(glob(os.path.join(path, "dicom/ct/bone_masks/*")))
    while len(images) > len(images_ct) or len(images) > len(bones):
        images.pop()
    while len(images_ct) > len(images) or len(images_ct) > len(bones):
        images_ct.pop()
    while len(bones) > len(images) or len(bones) > len(images_ct):
        bones.pop()
    while len(masks) > len(images):
        masks.pop()
    while len(thres) > len(images):
        thres.pop()
    return (images, images_ct, masks, thres, bones, preds)

def load_predict_data(path):
    """
    Use to load dataset for the prediction. The path should include data from a single subject
    """
    images = sorted(glob(os.path.join(path, "dicom/pet/images/*")))
    images_ct = sorted(glob(os.path.join(path, "dicom/ct/images/*")))
    masks = sorted(glob(os.path.join(path, "mediso/masks/*")))
    thres = sorted(glob(os.path.join(path, "dicom/pet/thres/*")))
    bones = sorted(glob(os.path.join(path, "dicom/ct/bone_masks/*")))
    while len(images) > len(images_ct) or len(images) > len(bones):
        images.pop()
    while len(images_ct) > len(images) or len(images_ct) > len(bones):
        images_ct.pop()
    while len(bones) > len(images) or len(bones) > len(images_ct):
        bones.pop()
    while len(masks) > len(images):
        masks.pop()
    while len(thres) > len(images):
        thres.pop()
    return (images, images_ct, masks, thres, bones)

def merge_pet_ct_images(path):
    """
    Applies the fused PET overlay on CT and saves it.
    The input path should contain resampled PET and CT slices from the same patient.
    """
    images = sorted(glob(os.path.join(path, "dicom/pet/images/*")))
    images_ct = sorted(glob(os.path.join(path, "dicom/ct/images/*")))
    num_slices = min(len(images), len(images_ct))

    if not os.path.isdir(os.path.join(path, "dicom/pet_ct/")):
        os.makedirs(os.path.join(path, "dicom/pet_ct/"))

    for i in range(num_slices):
        path_petct = os.path.join(path, f"dicom/pet_ct/{i}.png")
        x = cv2.imread(images[i], cv2.IMREAD_GRAYSCALE)
        x = cv2.resize(x, (256, 256))
        x_ct = cv2.imread(images_ct[i], cv2.IMREAD_GRAYSCALE)
        x_ct = cv2.resize(x_ct, (256, 256))
        cv2.imwrite(path_petct, (x + x_ct))


def read_image(path):
    """
    reads and stores the resampled input PET or CT image.
    """
    path = path.decode()
    x = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    x = cv2.resize(x, (256, 256))
    x = x/255.0
    x = np.expand_dims(x, axis=-1)
    return x

def read_mask(path):
    """
        reads and stores the input mask.
    """
    path = path.decode()
    x = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    x = cv2.resize(x, (256, 256))
    x = x/255.0
    x = np.expand_dims(x, axis=-1)
    return x

def tf_parse_label(y):
    """
    Utilizes TensorFlow to parse the sliced mask
    """
    def _parse(y):
        y = read_mask(y)
        return y

    y = tf.numpy_function(_parse, [y], tf.float64)
    y.set_shape([256, 256, 1])
    return y

def tf_parse(x, y):
    """
        Utilizes TensorFlow to parse the sliced input and mask
    """
    def _parse(x, y):
        x = read_image(x)
        y = read_mask(y)
        return x, y

    x, y = tf.numpy_function(_parse, [x, y], [tf.float64, tf.float64])
    x.set_shape([256, 256, 1])
    y.set_shape([256, 256, 1])
    return x, y

def tf_parse_multi(x, x_ct, x_bone):
    """
        Utilizes TensorFlow to parse the sliced dual channel input and mask
    """
    def _parse(x, x_ct, x_thres):
        x = read_image(x)
        x_ct = read_image(x_ct)
        x_thres = read_image(x_thres)
        return x, x_ct, x_thres

    x, x_ct, x_bone = tf.numpy_function(_parse, [x, x_ct, x_bone], [tf.float64, tf.float64, tf.float64])
    x.set_shape([256, 256, 1])
    x_ct.set_shape([256, 256, 1])
    x_bone.set_shape([256, 256, 1])
    return x, x_ct, x_bone

def tf_dataset(x, y, batch=8):
    """
    Utilizes TensorFlow to load and parse the data from input slices for single channel input
    """
    dataset = tf.data.Dataset.from_tensor_slices((x, y))
    dataset = dataset.map(tf_parse)
    dataset = dataset.batch(batch)
    dataset = dataset.repeat()
    return dataset

def tf_dataset_dual(x, x_ct, y, batch=8):
    """
        Utilizes TensorFlow to load and parse the data from input slices for dual channel input
    """
    dataset1 = tf.data.Dataset.from_tensor_slices((x, x_ct))
    dataset1 = dataset1.map(tf_parse)
    dataset2 = tf.data.Dataset.from_tensor_slices(y)
    dataset2 = dataset2.map(tf_parse_label)
    dataset = tf.data.Dataset.zip((dataset1, dataset2)).batch(batch).repeat()
    return dataset

def tf_dataset_multi(x, x_ct, x_bone, y, batch=8):
    """
        Utilizes TensorFlow to load and parse the data from input slices for multi channel input
    """
    dataset1 = tf.data.Dataset.from_tensor_slices((x, x_ct, x_bone))
    dataset1 = dataset1.map(tf_parse_multi)
    dataset2 = tf.data.Dataset.from_tensor_slices(y)
    dataset2 = dataset2.map(tf_parse_label)
    dataset = tf.data.Dataset.zip((dataset1, dataset2)).batch(batch).repeat()
    return dataset
