# PET-CT-U-Net

The U-Net based network, named PET-CT-U-Net, takes resampled PET and CT modalities as separate input channels and gets fit and trained by GT masks to predict slice based masks as output. Furthermore, thresholded masks based on PET and CT modalities are generated to complement the predicted masks for quantitative as well as qualitative analyses. To develop the automated segmentation unit, Google’s TensorFlow V.2.0 and Keras libraries are utilized to create a convolutional neural network (CNN) based on the U-Net architecture. 

## Requirements

The following libraries need to be installed as the dependencies.

* TensorFlow 2.0
* Keras
* PyRadiomics
* SciKit-Learn

## Scripts

1. The model.py defines three alternative U-Net based architectures: Single-, Dual-, and Multi- channels.

2. The train.py script is used to train and fit the models using TensorFlow and Keras modules.

3. The data.py defines functions and utilities related to initalize and configure the datasets to load tensor slices for train, validate, and test purposes.

4. The predict.py script provides the predicted lables for the test cohort.

5. The radiomics_pipeline.py takes advantage of PyRadiomics library to calculate radiomics features based on U-Net predicted masks.

## The Input Directory Structure
The input data directory should be structured as follows:

```
|-- input_data_directory                         
     |-- Subject_1                      
       	|-- dicom
               |-- pet
                    |-- orig
                         000000A
                         000000B
                         ...
                    |-- images
                         0.png
                         1.png
                         ...
                    |-- thres
                         0.png
                         1.png
                         ...
               |-- ct
                    |-- orig
                         000000A
                         000000B
                         ...
                    |-- images
                         0.png
                         1.png
                         ...
          |-- mediso
               |-- masks
                    0.png
                    1.png
                    ...

     |-- Subject_2
	    ...........	
     |-- Subject_3
	    ...........                                            
     ...........                                     
     |-- Subject_xx                                    
   
```

## Methods

The segmentation network defines two different models based on input channels: single and dual. Figure 1 illustrates a simplified schematic of PET-CT-U-Net.

![](images/PET-CT-U-Net.png)

In the first training step, the U-Net based model is trained and fit using PET and the combinations of PET and CT. As a result, two alternative models are trained, a model based on only PET and a model based on both PET and CT images respectively. The 40%-SUV<sub>MAX</sub> masks are used to set a baseline for performance analysis of the segmentation network. The U-Net model, developed in Python V.3.6 and utilized TensorFlow and Keras libraries, consists of seven encoding as well as seven decoding steps connected via a bridge layer. The input image sizes are set to 256×256 and the filter numbers are as follows: 16, 32, 48, 64, 128, 256, 480, 512. Thus, the resolution levels are customized compared to the original U-Net architecture. At each encoding iteration, we loop over the filter numbers and apply a 2D convolution block followed by a 2D max pooling. Then the bridge layer comes to action by applying a single 2D convolution block. Afterwards, the decoding step loops over the reversed filter numbers and applies a 2×2 upsampling followed by a 2D convolution block at each iteration. Finally, a sigmoid activation layer is applied after 1×1 2D convolution to end up with the output binary image. The 2D convolution block is composed of two 3×3 convolutions, each consisting of a batch normalization and a rectified linear unit (ReLU) activation. 

Because of the imbalance in the number of pixels in GT masks and background, weighted binary cross-entropy is used as the loss function between GT and background images. The segmentation quality metrics are measured as precision, recall, and the Dice coefficient of the predicted and GT masks. The networks are further tuned with different values for hyperparameters such as batch size (values: 8 and 16), learning rate (values: 0.0001, 0.001, 0.01, and 0.1), and up to 60 epochs. First, the train\_test\_split function of the model\_selection class of Scikit-Learn library is applied to subdivide the training cohort into interim train and validation subsets. Consecutively, the Dice coefficient of the predicted masks and GT labels (encoded as 2D binary images) is determined to fit the model. This process is repeated until the maximum number of epochs is reached or the early stopping criteria (using TensorFlow's EarlyStopping function with inputs: monitor=validation\_loss and patience=10) are met. Then, the fitted model is used to predict the masks for the held-out test set. Finally, the generated masks will be analyzed quantitatively (using Dice coefficients) as well as qualitatively (by an experienced NM expert). To prepare the input dataset for the therapy response prediction, the best predicted mask is used to calculate radiomics features. To this end, for each patient, the predicted mask is applied to input images to calculate features using PyRadiomics library and end up with patient-specific feature vectors. 

## Sample Results

Figure 2 provides a qualitative outline of the segmentation results, comparing original input channels and multi-channel U-Net model prediction. As the results suggest, the U-Net predicted mask performs reasonably well as compared to the GT mask. Moreover, the U-Net prediction outperformed the thresholding based mask, specifically, for the identification of the physiological uptake (e. g., in livers and kidneys) which is considered as one of the challenging tasks for computer based algorithms [1, 2]. Furthermore, the proposed automated segmentation performs well in predicting bone metastasis uptakes.

![](images/U-Net-Slices.png)

The PET-CT-U-Net applies both thresholding and U-Net based methods on both PET and CT modalities to end up with predicted masks (figure 2). For further analysis steps, these predicted masks are applied to calculate radiomics features (RFs) for PET and CT modalities.

## Cite This Work

Cite this work as:

Moazemi S., Essler M., Schultz T., Bundschuh R.A. (2021) Predicting Treatment Response in Prostate Cancer Patients Based on Multimodal PET/CT for Clinical Decision Support. In: Syeda-Mahmood T. et al. (eds) Multimodal Learning for Clinical Decision Support. ML-CDS 2021. Lecture Notes in Computer Science, vol 13050. Springer, Cham. https://doi.org/10.1007/978-3-030-89847-2_3

The U-Net based segmentation unit, was inspired by a single channel U-Net model originally implemented by Tomar, N.K. for polyp segmentation (https://idiotdeveloper.com/polyp-segmentation-using-unet-in-tensorflow-2/). The original U-Net model was introduced by Ronneberger et al. [3] in 2015 an has been adapted for many medical image segmentation tasks therafter.

## References

[1] Moazemi S, Khurshid Z, Erle A, Lütje S, Essler M, Schultz T, Bundschuh RA. Machine Learning Facilitates Hotspot Classification in PSMA-PET/CT with Nuclear Medicine Specialist Accuracy. Diagnostics (Basel). 2020 Aug 22;10(9):622. doi: 10.3390/diagnostics10090622. PMID: 32842599; PMCID: PMC7555620.

[2] Erle A, Moazemi S, Lütje S, Essler M, Schultz T, Bundschuh RA. Evaluating a Machine Learning Tool for the Classification of Pathological Uptake in Whole-Body PSMA-PET-CT Scans. Tomography. 2021; 7(3):301-312. doi: 10.3390/tomography7030027

[3] Ronneberger O., Fischer P., Brox T. (2015) U-Net: Convolutional Networks for Biomedical Image Segmentation. In: Navab N., Hornegger J., Wells W., Frangi A. (eds) Medical Image Computing and Computer-Assisted Intervention – MICCAI 2015. MICCAI 2015. Lecture Notes in Computer Science, vol 9351. Springer, Cham. https://doi.org/10.1007/978-3-319-24574-4_28
